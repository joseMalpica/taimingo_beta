import {Platform, MenuController, Nav, App, NavController} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from  '../pages/home/home';
import { TabsPage } from "../pages/tabs/tabs";
import { LoginPage } from '../pages/login/login';
import { Storage } from '@ionic/storage';
import { Identity } from '../providers/identity';
import { Events } from 'ionic-angular';
import {OneSignal} from '@ionic-native/onesignal';
import {IntroPage} from "../pages/intro/intro";
import { RootPage } from "../pages/root/root";

@Component({
    selector: 'app-root',
    templateUrl: 'app.html'
})
export class MyApp {

    @ViewChild(Nav) nav:Nav;

    // make WalkthroughPage the root (or first) page
    public rootPage:any = RootPage;
    googleProjectId = "771313265102";
    appId ="f6e01c64-7fcf-4579-88b3-035d55af1119";

    pages:Array<{title: string, icon: string, component: any}>;
    pushPages:Array<{title: string, icon: string, component: any}>;

    constructor(public platform:Platform,
                public app:App,
                public menu:MenuController,
                public events:Events,
                public identity:Identity,
                private oneSignal: OneSignal,
                public storage:Storage,
                public SplashScreen : SplashScreen){

        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            // StatusBar.styleDefault();
            //   BackgroundMode.enable();

            this.storage.ready().then(() =>{
                this.initOneSignal();
                this.storage.get('loginState').then((value)=>{
                    if(value == true) {
                        //Comprobar si es la primera vez que abre la app
                        this.storage.get('introShown').then((result)=>{
                            if(result  == true){
                                this.rootPage = TabsPage;
                            }else{
                                this.rootPage = IntroPage;
                                this.storage.set('introShown', true);
                            }
                        });
                        this.SplashScreen.hide();
                    }else{
                        this.rootPage = LoginPage;
                        this.SplashScreen.hide();
                    }
                }).catch(()=>{
                    this.rootPage = LoginPage;
                    this.SplashScreen.hide();
                })
            });
        });

        try {
            events.subscribe('identity:after_validate_identity', success => {
                if (success === true) {
                    this.nav.setRoot(TabsPage);
                    this.SplashScreen.hide();
                } else {
                    this.nav.setRoot(LoginPage);
                    this.identity.logout();
                    this.SplashScreen.hide();
                }
            });
        } catch (err){
            console.log(err);
            this.identity.logout();
            this.nav.setRoot(LoginPage);
            this.SplashScreen.hide();
        }

        events.subscribe('identity:after_logout', () => {
            this.menu.close();
        });


        this.identity = identity;
        //this.identity.main_page = {component: TabsNavigationPage};
        this.identity.login_page = {component: LoginPage};

        this.pages = [
            {title: 'Dashboard', icon: 'home', component: TabsPage},
        ];
        /*
                this.pushPages = [
                    {title: 'Contenido', icon: 'grid', component: ContentPage},
                    {title: 'Configuración', icon: 'settings', component: SettingsPage},
                    {title: 'Chat', icon: 'none', component: ChatPage}
                ];



                        this.pages = [
                            {title: 'Dashboard', icon: 'home', component: TabsPage},
                            {title: 'Forms', icon: 'create', component: FormsPage},
                            {title: 'Home', icon: '', component: TabsPage},
                        ];

                        this.pushPages = [
                            {title: 'Layouts', icon: 'grid', component: LayoutsPage},
                            {title: 'Settings', icon: 'settings', component: SettingsPage},
                            {title: 'Chat', icon: 'none', component: ChatPage}
                        ];
                 */
    }

    initOneSignal(){
        console.log("iniciando OneSignal");
        this.oneSignal.startInit(this.appId, this.googleProjectId);

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
            // do something when notification is received
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
            // do something when a notification is opened
        });

        this.oneSignal.endInit();
        console.log("========>>>>>  Terminando OneSignal");
        /*
        // OneSignal Code start:
        // Enable to debug issues:
        // window["plugins"].OneSignal.setLogLevel({logLevel: 4, visualLevel: 4});

        var notificationOpenedCallback = function(jsonData) {
            console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };

        window["plugins"].OneSignal
            .startInit(this.appId, this.googleProjectId)
            .handleNotificationOpened(notificationOpenedCallback)
            .endInit();
    */
    }


    openPage(page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // navigate to the new page if it is not the current page
        this.nav.push(page);

    }

    pushPage(page) {
        // close the menu when clicking a link from the menu
        this.menu.close();
        // rootNav is now deprecated (since beta 11) (https://forum.ionicframework.com/t/cant-access-rootnav-after-upgrade-to-beta-11/59889)
        this.nav.push(page.component);
    }

    /*
    check() {
        this.identity.check().subscribe(r=>console.log(r));
    }
    */
}
