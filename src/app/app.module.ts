import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';

//-------providers
import { FreedomApi } from "../providers/freedom-api";
import { Identity } from "../providers/identity";
import { HttpModule } from '@angular/http';

//-------pages
import { LoginPage, HomePage, RootPage, TabsPage, ProfileViewPage, SignupPage,
    ProfilePage, AddCardPage, AutocompletePage, ShowPucPage, AddPucPage,
    PackagePage, CollectionCodePage, SharePackagePage, SendOtherPucPage,
    ValidateSmsPage, PackageHistoryPage, RecoverPasswordPage, HandDeliveryPage,
    IntroPage, InfoModalPage} from "../pages/index.pages";

//-------directives
import { ParallaxHeaderDirective } from '../directives/parallax-header/parallax-header';

//------plugins
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { SocialSharing } from '@ionic-native/social-sharing';
import { OneSignal } from '@ionic-native/onesignal';
import { FileTransfer } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
//import { GoogleMaps } from '@ionic-native/google-maps';

@NgModule({
    declarations: [
        MyApp,
        ParallaxHeaderDirective,
        //-------pages
        LoginPage, HomePage, RootPage, TabsPage, ProfileViewPage, SignupPage,
        ProfilePage, AddCardPage, AutocompletePage, ShowPucPage, AddPucPage,
        PackagePage, CollectionCodePage, SharePackagePage, SendOtherPucPage,
        ValidateSmsPage, PackageHistoryPage, RecoverPasswordPage, HandDeliveryPage,
        IntroPage, InfoModalPage
    ],
    imports: [
        BrowserModule,
        HttpModule,
        IonicStorageModule.forRoot({
            name: '__taimingoDb',
            driverOrder: ['indexeddb', 'sqlite', 'websql']
        }),
        IonicModule.forRoot(MyApp,{
            tabsPlacement: 'top',
            tabsHideOnSubPages: true,
        })
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        //-------pages
        LoginPage, HomePage, RootPage, TabsPage, ProfileViewPage, SignupPage,
        ProfilePage, AddCardPage, AutocompletePage, ShowPucPage, AddPucPage,
        PackagePage, CollectionCodePage, SharePackagePage, SendOtherPucPage,
        ValidateSmsPage, PackageHistoryPage, RecoverPasswordPage, HandDeliveryPage,
        IntroPage, InfoModalPage
    ],
    providers: [
        Geolocation,
        OneSignal,
        //GoogleMaps,
        NativeGeocoder,
        SocialSharing,
        FileTransfer,
        File,
        StatusBar,
        SplashScreen,
        FreedomApi,
        Identity,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {}
