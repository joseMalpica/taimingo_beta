

var response = null;

	function successCard(_responseData) {
		var promise = new Promise(function(resolve, reject) {
		  // do a thing, possibly async, then…
		  if (response!=null) {
		    resolve(response);
		  }
		  else {
		    response = _responseData;
				resolve(_responseData);
		  }
		});
		return promise

	};
	function errorCard(_errorResponseData){
		console.log(_errorResponseData);
	};

	function callCreateToken(cardNumber, holderName, expirationYear, expirationMonth, cvv, postalCode, calle, numero, municipio, state) {
		console.log(cardNumber);
		var _id = '', _apiKey = '', _data = null, _dataCard = '';
		var promise = new Promise(function(resolve, reject) {
		  // do a thing, possibly async, then…
			OpenPay.setSandboxMode(true);
			_id = "mmnsclavs9ccexnksvwr";
			_apiKey = "pk_9c95b8dbaee94ec3830f199687c881ba";
			OpenPay.setId(_id);
			OpenPay.setApiKey(_apiKey);
			OpenPay.token.create({
						"card_number":cardNumber,
						"holder_name":holderName,
						"expiration_year":expirationYear,
						"expiration_month":expirationMonth,
						"cvv2":cvv,
						"address":{
							 "city":municipio,
							 "line2":numero,
							 "postal_code":postalCode,
							 "line1":calle,
							 "state":state,
							 "country_code":"MX"
						}
			}, successCard, errorCard);
			setTimeout(() => {
	      successCard().then((data)=>{console.log(data)});
				resolve(response);
	    }, 2000);
		});
		return promise



	};
