import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';

declare var callCreateToken;
@Component({
  selector: 'page-add-card',
  templateUrl: 'add-card.html',
})
export class AddCardPage {

  noCard:string;
  nameCard: any;
  cvvCard: any;
  monthCard: string;
  yearCard: string;
  addressCalle: any;
  addressNumero: any;
  addressCp: any;
  addressMunicipio: any;
  addresEstado: any;
  private switch = 1;
  //1 -> Card data
  //2 -> Address data

  months = [
    {"id": 1, "name": "Enero"},
    {"id": 2, "name": "Febrero"},
    {"id": 3, "name": "Marzo"},
    {"id": 4, "name": "Abril"},
    {"id": 5, "name": "Mayo"},
    {"id": 6, "name": "Junio"},
    {"id": 7, "name": "Julio"},
    {"id": 8, "name": "Agosto"},
    {"id": 9, "name": "Septiembre"},
    {"id": 10, "name": "Octubre"},
    {"id": 11, "name": "Noviembre"},
    {"id": 12, "name": "Diciembre"},
  ]
  years = [
    {"id": 2001, "value": "01"  },
    {"id": 2002, "value": "02"  },
    {"id": 2003, "value": "03"  },
    {"id": 2004, "value": "04"  },
    {"id": 2005, "value": "05"  },
    {"id": 2006, "value": "06"  },
    {"id": 2007, "value": "07"  },
    {"id": 2008, "value": "08"  },
    {"id": 2009, "value": "09"  },
    {"id": 2010, "value": "10"  },
    {"id": 2011, "value": "11"  },
    {"id": 2020, "value": "20"  },
  ]

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad AddCardPage');

  }

  switchScreen(){
    if(this.switch == 1){
      this.switch = 2;
    }else{
      this.switch = 1;
    }
  }
  addCard(){
    //callCreateToken(this.noCard, this.nameCard, this.yearCard, this.monthCard, this.cvvCard, this.addressCp, this.addressCalle, this.addressNumero, this.addressMunicipio, this.addresEstado).then((data)=>{console.log(data)});
    let loader = this.loadingCtrl.create({
          content: "Guardando datos",
          duration: 3000
        });
        loader.present();
    callCreateToken("4111111111111111", "Juan Perez Ramirez", "20", "12", "110", "76900", "Av 5 de Febrero", "58", "Queretaro", "Queretaro").then((data)=>{
      console.log(data);
      setTimeout(() => {
	      loader.dismiss();
	    }, 1500);

    });

  }

}
