import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, Platform } from 'ionic-angular';

//pages
import { ProfilePage, SharePackagePage } from "../index.pages";

//plugins
import { Storage } from '@ionic/storage';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

//-------providers
import { Identity } from "../../providers/identity";
import {FreedomApi} from "../../providers/freedom-api";

declare let cordova: any;

@Component({
    selector: 'page-collection-code',
    templateUrl: 'collection-code.html',
})
export class CollectionCodePage {

    private puc: any;
    collectionCodeImageUrl: any;
    imagen:any;
    myPos:any;
    packId:any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private platform: Platform,
                private _id: Identity,
                private _fa: FreedomApi,
                public toastCtrl: ToastController,
                private transfer: FileTransfer,
                private file: File,
                public storage: Storage) {
        this.puc = navParams.get( "puc" );
        this.myPos = this.navParams.get("myPos");
        this.packId = this.navParams.get("packId");
        this.collectionCodeImageUrl = 'http://' + this.navParams.get("image");
        console.log( this.collectionCodeImageUrl );
    }

    ionViewDidLoad() {
        this.platform.ready().then(()=>{
            if(this.platform.is('cordova')){
                this.downloadImagen();
            }else{
                console.log("Se guarda la imagen we!");
            }
        });


    }
    goProfile(){
        this.navCtrl.push( ProfilePage );
    }
    ionViewCanEnter(){
        this.storage.ready().then(() =>{
            this.storage.get('loginState').then((value)=>{
                if( value === true ){
                    return true;
                }else{
                    return false;
                }
            });
        });
    }

    /* Download image of collection code & save in the storage */
    downloadImagen() {
        const fileTransfer: FileTransferObject = this.transfer.create();
        //const url = 'https://www.definicionabc.com/wp-content/uploads/Im%C3%A1gen-Vectorial.jpg';
        const url = this.collectionCodeImageUrl;
        fileTransfer.download(url, cordova.file.dataDirectory + 'imagen.jpg').then((entry) => {
            //this.presentToast('download complete: ' + entry.toURL());
            this.imagen = entry.toURL();
        }, (error) => {
            // handle error
            this.presentToast(error);
        });
        this.file.checkFile(cordova.file.dataDirectory, 'imagen.jpg').then((res)=>{
            //this.presentToast(res);
        })
    }

    presentToast(msg){
        let toast = this.toastCtrl.create({
            message: msg,
            showCloseButton: true,
            //duration: 3000
        });
        toast.present();
    }




    //-------Navigation
    goSharePackage(){
        this.navCtrl.push( SharePackagePage, {"puc": this.puc, "myPos": this.myPos, "packId": this.packId} );
    }


}
