import { Component } from '@angular/core';
import {NavController, NavParams, Platform, ToastController} from 'ionic-angular';

//--------plugins
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { NativeGeocoder, NativeGeocoderForwardResult } from '@ionic-native/native-geocoder';
import { Storage } from '@ionic/storage';

//------Providers
import { FreedomApi } from "../../providers/freedom-api";

declare var google: any;

@Component({
    selector: 'page-hand-delivery',
    templateUrl: 'hand-delivery.html'
})
export class HandDeliveryPage {

    package:any;
    screen = 1;
    map: google.maps.Map;
    myPos: any;
    puc: any;
    markerIam: any;


    address = {
        "street_number": "",
        "route": "",
        "postal_code": "",
        "sublocality": "",
        "locality": "",
        "estate": "",
        "country": ""
    };


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private platform: Platform,
                private storage: Storage,
                private _fa: FreedomApi,
                public toastCtrl: ToastController,
                public geocoder: NativeGeocoder,
                public geolocation: Geolocation){
    }

    ionViewDidLoad(){
        this.myPos = this.navParams.get("myPos");
        this.puc = this.navParams.get("puc");
        this.package = this.navParams.get("package");
        //console.log(this.package);
        //console.log(this.myPos);
        this.platform.ready().then(()=>{
            this.loadMap();
        });
    }

    getPosition():any{
        /*this.geolocation.getCurrentPosition()
          .then(response => {
            this.loadMap(response);
          })
          .catch(error =>{
            console.log(error);
          })*/

    }

    loadMap(){
        // create a new map by passing HTMLElement
        let mapEle: HTMLElement = document.getElementById('mapa');
        // create LatLng object
        let pucLatLng = {lat: this.puc.position.lat, lng: this.puc.position.lng};
        let myLatLng = {lat: this.myPos.lat, lng: this.myPos.lng};
        // create map
        this.map = new google.maps.Map(mapEle, {
            center: pucLatLng,
            zoom: 12
        });

        google.maps.event.addListenerOnce(this.map, 'idle', () => {
            let cityCircle = new google.maps.Circle({
                strokeColor: '#FF0000',
                strokeOpacity: 0.8,
                strokeWeight: 2,
                fillColor: '#FF0000',
                fillOpacity: 0.35,
                map: this.map,
                center: pucLatLng,
                radius: 5000
            });
            let markerPuc = new google.maps.Marker({
                position: pucLatLng,
                map: this.map,
                title: 'Hello World!',
                icon: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
                draggable: false
            });
            //mapEle.classList.add('show-map');
            this.markerIam = new google.maps.Marker({
                position: myLatLng,
                map: this.map,
                title: 'Hello World!',
                draggable: true
            });
            this.markerIam.getPosition(function(position) {
                console.log(JSON.stringify(position));
            });
            mapEle.classList.add('show-map');
        });

        /*google.maps.event.addListener(this.markerIam, 'draged', function () {
            console.log('movido');
        });*/
    }

    doAction(){
        var flagSublocality = false;
        var flagLocality = false;
        var flagAdministrativeArea = false;
        var flagCountry= false;
        var address = {
            "street_number": "",
            "route": "",
            "postal_code": "",
            "sublocality": "",
            "locality": "",
            "estate": "",
            "country": ""
        };
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode(()=>{

        });

        geocoder.geocode({
            "location": {"lat": this.markerIam.getPosition().lat(), "lng": this.markerIam.getPosition().lng()}
        }, function(results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                //console.log(results[0].address_components);
                results[0].address_components.forEach((data, i)=>{
                    data.types.forEach(type=>{if(type == "sublocality"){flagSublocality = true }});
                    data.types.forEach(type=>{if(type == "locality"){flagLocality = true }});
                    data.types.forEach(type=>{if(type == "administrative_area_level_1"){flagAdministrativeArea = true }});
                    data.types.forEach(type=>{if(type == "country"){flagCountry = true }});
                    //"administrative_area_level_1"
                    if(data.types == "street_number"){
                        //número
                        var input = document.getElementById('streetNumber');
                        input.setAttribute("value",  data.short_name);
                        address.street_number = data.short_name;
                        //console.log(address.street_number);
                    }else if(data.types == "route"){
                        //calle
                        var input = document.getElementById('route');
                        input.setAttribute("value",  data.short_name);
                        address.route = data.short_name;
                    }else if(data.types == "postal_code"){
                        //código postal
                        address.postal_code = data.short_name;
                    }
                    if(flagSublocality == true){
                        //colonia
                        var input = document.getElementById('sublocality');
                        input.setAttribute("value",  data.short_name);
                        address.sublocality = data.short_name;
                        flagSublocality = false;
                    }else if(flagLocality == true){
                        //municipio
                        address.locality = data.short_name;
                        flagLocality = false;
                    }else if(flagAdministrativeArea == true){
                        //Estado
                        var input = document.getElementById('locality');
                        input.setAttribute("value",  data.long_name);
                        address.estate = data.long_name;
                        flagAdministrativeArea = false;
                    }else if(flagCountry == true){
                        //Pais
                        var input = document.getElementById('country');
                        input.setAttribute("value",  data.long_name);
                        address.country = data.long_name;
                        flagCountry = false;
                    }

                });
                //results[1].formatted_address
                //console.log("number: "+results[0].address_components);
                //console.log(results[1].formatted_address);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
        this.screen = 2;
    }

    //Enviar informacion al servidor
    sendData(){
        /*  Get data from the form */
        var inputNumber = document.getElementById('streetNumber');
        var inputRoute = document.getElementById('route');
        var inputSubloccality = document.getElementById('sublocality');
        var inputLocality = document.getElementById('locality');
        var inputCountry = document.getElementById('country');
        var address =  inputNumber.getAttribute('value')+ " " + inputRoute.getAttribute('value')+ ", " + inputSubloccality.getAttribute('value')+ ", " + inputLocality.getAttribute('value')+ ", " + inputCountry.getAttribute('value');
        //var geocoder = new google.maps.Geocoder();
        /*  Obtener lat and lng a partir de direccion
         * Datos de la nueva direccion, donde se entregara el paquete */
        this.geocoder.forwardGeocode(address).then((coordinates: NativeGeocoderForwardResult)=>{
            //Calcular distancia
            var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(this.puc.position.lat, this.puc.position.lng),
                new google.maps.LatLng(coordinates.latitude, coordinates.longitude));

            //Enviar datos
            this._fa.post('taimingo/hand-delivery',{'HandDelivery' : {
                        'package_id': this.package.id,
                        'pick_up_center_id':this.puc._id,
                        'destination':{'lat':coordinates.latitude , 'lng': coordinates.longitude },
                        'distance': distance
                    }
            }).subscribe((data)=>{
                //this.presentToast(data)
                this.navCtrl.pop();
            });
        }).catch((err)=>{
            this.presentToast(JSON.stringify(err));
        });
         /*Terminar */



        //console.log('package_id: '+this.package.id);
        //console.log('pick_up_center_id: '+this.puc.id);
        //console.log('destination-> lat: '+this);








    }

    presentToast(msg) {
        let toast = this.toastCtrl.create({
            message: msg,
            showCloseButton: true
        });
        toast.present();
    }

}





