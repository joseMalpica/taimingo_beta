import { identifierModuleUrl } from '@angular/compiler/compiler';
import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActionSheetController, AlertController, App, LoadingController, NavController, Platform, ToastController, ModalController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';

//-----plugins
import { Storage } from '@ionic/storage';

//------providers
import { FreedomApi } from "../../providers/freedom-api";

//------pages
import { ShowPucPage, AddPucPage, PackagePage, PackageHistoryPage } from "../index.pages";

declare var google: any;

@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
    addressElement: HTMLInputElement = null;
    listSearch: string = '';
    map: any;
    marker: any;
    loading: any;
    search: boolean = false;
    error: any;
    switch: string = "map";
    private myPucs:any;
    private allPucs: any;
    private pos:any;
    private packages: any;
    private infoWindow;

    constructor(
        private navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public app: App,
        public nav: NavController,
        public zone: NgZone,
        public platform: Platform,
        public alertCtrl: AlertController,
        public storage: Storage,
        public modalCtrl: ModalController,
        public actionSheetCtrl: ActionSheetController,
        public geolocation: Geolocation,
        public _fa: FreedomApi
    ) {
        this.platform.ready().then(() => {
            this.loadMaps()
        });

    }

    ionViewWillEnter(){
        this.getPucs();
        this.getPackages();
    }

    ionViewDidLoad(){
        document.getElementById("btnAddPuc").style.display = "none";
    }

    public getPucs(){
        //get my PUC's
        this._fa.get( 'taimingo/pick-up-center-customer' ).subscribe((data)=>{
            //console.log(data.data.models);
            this.myPucs = data.data.models;
        });
        //Get all PUC's
        this._fa.get( 'taimingo/pick-up-center' ).subscribe(( data )=>{
            //console.log( data.data.models );
            this.allPucs = data.data.models;
            this.allPucs.forEach( puc => {
                this.addMarker(puc.position, puc.name, true, puc);
            });
        });
    }

    getPackages(){
        //this.packages
        this._fa.get('taimingo/package/start' ).subscribe((data)=>{
            this.packages = data.data;
            //console.log(this.packages);
        });
    }

    loadMaps() {
        if (!!google) {
            this.initializeMap();
            this.getCurrentPosition();
            this.initAutocomplete();
        } else {
            this.errorAlert('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
        }
    }

    alertReload(){
        let confirm = this.alertCtrl.create({
            title: 'Upps!',
            message: 'No hemos podido detectar tu ubicacion, deseas volver a intentar?',
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('desacuerdo clickeado');
                    }
                },
                {
                    text: 'Ok',
                    handler: () => {
                        this.getCurrentPosition();
                    }
                }
            ]
        });
        confirm.present();
    }

    errorAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        this.loadMaps();
                    }
                }
            ]
        });
        alert.present();
    }

    initAutocomplete(): void {
        // reference : https://github.com/driftyco/ionic/issues/7223
        this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
        this.createAutocomplete(this.addressElement).subscribe((location) => {
            //console.log('Searchdata', location);
            let options = {
                center: location,
                zoom: 10
            };
            this.map.setOptions(options);
            //this.addMarker(location, "Mi busqueda");

        });
    }

    createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
        const autocomplete = new google.maps.places.Autocomplete(addressEl);
        autocomplete.bindTo('bounds', this.map);
        return new Observable((sub: any) => {
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                const place = autocomplete.getPlace();
                if (!place.geometry) {
                    sub.error({
                        message: 'Autocomplete returned place with no geometry'
                    });
                } else {
                    //console.log('Search Lat', place.geometry.location.lat());
                    //console.log('Search Lng', place.geometry.location.lng());
                    sub.next(place.geometry.location);
                    //sub.complete();
                }
            });
        });
    }

    initializeMap() {
        var myPosition: any;
        this.zone.run(() => {
            var mapEle = this.mapElement.nativeElement;
            this.geolocation.getCurrentPosition()
                .then(position => {
                    myPosition = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    }
                })
                .catch(error=>{
                    console.log(error);
                });
            this.map = new google.maps.Map(mapEle, {
                zoom: 12,
                center: myPosition,
            });
            let markers = [];
            google.maps.event.addListenerOnce(this.map, 'idle', () => {
                google.maps.event.trigger(this.map, 'resize');
                mapEle.classList.add('show-map');
                this.bounceMap(markers);
            });
            google.maps.event.addListener(this.map, 'bounds_changed', () => {
                this.zone.run(() => {
                    this.resizeMap();
                });
            });
        });
    }

    //http://stackoverflow.com/questions/19304574/center-set-zoom-of-map-to-cover-all-visible-markers
    bounceMap(markers) {
        let bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        this.map.fitBounds(bounds);
    }
    resizeMap() {
        setTimeout(() => {
            google.maps.event.trigger(this.map, 'resize');
        }, 200);
    }

    showToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }

    // go show currrent location
    getCurrentPosition() {
        this.loading = this.loadingCtrl.create({
            spinner: 'dots',
            content: 'Buscando tu posición ...'
        });
        this.loading.present();
        let locationOptions = { timeout: 10000, enableHighAccuracy: true };
        this.geolocation.getCurrentPosition(locationOptions)
            .then(position => {
                    this.loading.dismiss().then(() => {
                        //console.log(position.coords.latitude, position.coords.longitude);
                        let myPos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        this.pos = {"lat": position.coords.latitude, "lng": position.coords.longitude};
                        let options = {
                            center: myPos,
                            zoom: 12
                        };
                        this.map.setOptions(options);
                        this.addMarker(myPos, "Mi posición");
                    });
                },
                (error) => {
                    this.loading.dismiss().then(() => {
                        //this.showToast('Location not found. Please enable your GPS!');
                        this.alertReload();
                        console.log(error);
                    });
                }
            )
    }

    addMarker(position, content, isPuc?, puc?) {
        var pinColor; //verde
        (isPuc == true ?  pinColor = "0DB605"  :  pinColor = "FE7569");
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: position,
            icon: pinImage,
            //draggable:true
        });
        if( isPuc ){
            this.addInfoWindowPuc(marker, content, puc);
        } else{
            this.addInfoWindow(marker, content);
        }
        return marker;
    }

    addInfoWindow(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: "<strong> "+ content + " </strong>"
        });
        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });
    }
    public  hola(){
        var pucId = document.getElementById('btnAddPuc').getAttribute("value");
        var newPuc:any;
        //get puc
        this.allPucs.forEach((puc)=>{
            if(puc._id == pucId){
                newPuc = puc;
            }
        });
        this.goShowPucPage( newPuc );
    }

    addInfoWindowPuc(marker, content, puc?) {
        var infoWindow = null;
        if(puc.aliasPuc == null){
            infoWindow = new google.maps.InfoWindow({
                content: "<strong> "+ content + " </strong> <br>" +
                "<span id='addPuc' style='color: #0DB605;float: left;font-size: 14px;font-weight: bold;'> Agregar PUC </span>"
            });
        }else{
            infoWindow = new google.maps.InfoWindow({
                content: "<strong> "+ content + " </strong> <br>" +
                "<span id='addPuc' style='color: #0DB605;float: left;font-size: 14px;font-weight: bold;'> Ir al PUC </span>"
            });
        }
        google.maps.event.addListener(marker, 'click', () => {
            //si hay ventana
            if(this.infoWindow != null){
                this.infoWindow.close();
            }
            this.infoWindow = infoWindow;
            this.infoWindow.open(this.map, marker);
            var btnAddPuc = document.getElementById('addPuc');
            btnAddPuc.onclick = function(){
                document.getElementById('btnAddPuc').setAttribute("value", puc._id);
                document.getElementById('btnAddPuc').click();
            };
            //infoWindow.close();
        });
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);

        setTimeout(() => {
            console.log('Async operation has ended');
            this.getPucs();
            this.getPackages();
            refresher.complete();
        }, 2000);
    }

    //---Navigation
    public goShowPucPage( puc ){
        this.navCtrl.push( ShowPucPage, { "pucId": puc._id, "myPos": this.pos } )
    }
    public goAddPuc(){
        //this.app.getRootNav().push( AddPucPage );
        this.navCtrl.push( AddPucPage, { "myPos": this.pos } );
    }
    public gotPackage( puc, pack ){
        //quitar el PUC porque vendra en el servicio siguiente
        this.navCtrl.push( PackagePage, { puc , pack , "myPos": this.pos } );
    }

    public goPackage( packId ){
        this.navCtrl.push( PackagePage, { packId, "myPos": this.pos } );
    }

    public goHistory(){
        let modal = this.modalCtrl.create(PackageHistoryPage);
        modal.present();
        //this.navCtrl.push(PackageHistoryPage);
    }

    testeando(){
        console.log("hola");
        this._fa.post('taimingo/hand-delivery',{"HandDelivery":{ "package_id":"59d5245836d3ba15925ab8f3", "pick_up_center_id":"5955414b968706248a239f01", "position":{"lat": 19.548373452442, "lng":-99.236278736267 }, "destination":{"lat": 19.548373452442, "lng": -99.236278736267} }}).subscribe((data)=>{
            console.log(data);
        });
    }

}

