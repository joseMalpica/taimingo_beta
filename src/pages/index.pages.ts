export { LoginPage } from "./login/login";
export { HomePage } from "./home/home";
export { ProfileViewPage } from "./profile-view/profile-view";
export { ProfilePage } from "./profile/profile";
export { RootPage } from "./root/root";
export { TabsPage } from "./tabs/tabs";
export { SignupPage } from "./signup/signup";
//export { ValidatePage } from "./validate/validate";
export { PackagePage } from "./package/package";
export { AutocompletePage } from "./autocomplete/autocomplete";
export { ShowPucPage } from "./show-puc/show-puc";
export { SharePackagePage } from "./share-package/share-package";
export { AddPucPage } from "./add-puc/add-puc";
export { CollectionCodePage } from "./collection-code/collection-code";
export { AddCardPage} from "./add-card/add-card";
export { SendOtherPucPage } from "./send-other-puc/send-other-puc";
export { ValidateSmsPage } from "./validate-sms/validate-sms";
export { PackageHistoryPage } from "./package-history/package-history";
export { RecoverPasswordPage } from "./recover-password/recover-password";
export { HandDeliveryPage } from "./hand-delivery/hand-delivery";
//export { PucPage } from "./puc/puc";
//export { TestPage } from "./test/test";
export { IntroPage } from "./intro/intro";
export { InfoModalPage } from "./info-modal/info-modal";
