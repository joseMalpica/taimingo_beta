import { Component } from '@angular/core';
import { NavController, NavParams, App, Platform } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {LoadingController} from "ionic-angular/index";
import { Storage } from '@ionic/storage';
//pages
import { TabsPage, SignupPage, RecoverPasswordPage, IntroPage } from "../index.pages";
//providers
import { FreedomApi } from "../../providers/freedom-api";
import { Identity } from "../../providers/identity";
//------plugins
import { OneSignal } from "@ionic-native/onesignal";

@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginPage {

    loading:any;
    login:FormGroup;
    Device = { uuid:"localhost", kind:"mobile",os:"ANDROID",version:"1.2.6"};

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public app:App,
                private platform: Platform,
                private _OneSignal: OneSignal,
                private storageSession:Storage,
                public loadingCtrl:LoadingController,
                private api: FreedomApi,
                private identity: Identity) {
        if(this.platform.is('cordova')){
            this.credentials();
        }
        this.login = new FormGroup({
            //email: new FormControl('finalmau@gmail.com', Validators.required),
            email: new FormControl('nona.rimacuna@yopmail.com', Validators.required),
            password: new FormControl('123123', Validators.required)
        });

    }

    ionViewDidLoad() {
    }

    private doLogin(){
        this.loading = this.loadingCtrl.create();
        this.loading.present();
        this.api.post('app/guest/login', { Customer: this.login.value, Device: this.Device } )
            .subscribe(
                response => {
                    console.log( response.ok );
                    if (response.ok === true) {
                        this.identity.loadData( response.data );
                        if (this.identity.isLogged()) {
                            this.login.reset();
                            this.storageSession.set( "loginState",true );
                            this.loading.dismiss();
                            //Comprobar si es la primera vez que abre la app
                            this.storageSession.get("introShown").then((result)=>{
                                console.log(result);
                                if(result === true){
                                    this.navCtrl.setRoot( TabsPage );
                                }else{
                                    this.navCtrl.setRoot( IntroPage );
                                    this.storageSession.set('introShown', true);
                                }
                            });

                        } else {
                            this.navCtrl.setRoot( LoginPage );
                        }
                    } else {
                        console.log( "bandera" );
                        this.loading.dismiss();
                        /*let alert = this.alertCtrl.create({
                          title: 'Inicio de sesión',
                          message: 'Eso no ha funcionado, por favor verifica tus datos',
                          buttons: ['Entendido']

                        });
                        alert.present().catch(() => {
                        });*/
                    }
                    //this.loading.dismiss();
                },
                error => {
                    console.log(error);
                    this.loading.dismiss();
                    /*let alert = this.alertCtrl.create({
                      title: 'Inicio de sesión',
                      message: 'Verifica tu conexión',
                      buttons: ['Entendido']
                    });
                    alert.present().catch(()=>{
                    });*/
                }
            );
    }



    credentials() {
        let aux;
        if(this.platform.is("ios")){
            aux = this.platform.versions();
            this.Device["os"] = "IOS" + aux.ios.str;
        }else{
            aux = this.platform.versions();
            this.Device["os"] = "ANDROID" + aux.android.str;
        }
        if(this.platform.is("tablet")){
            this.Device["kind"] = "tablet"
        }else {
            this.Device["kind"] = "mobile"
        }
        this._OneSignal.getIds().then((val)=> {
            this.Device["uuid"] = val.userId;
            this.storageSession.set("Device", this.Device);
        })
    }

    private goToSignup(){
        this.navCtrl.push( SignupPage );
    }

    private goToForgotPassword(){
        this.navCtrl.push( RecoverPasswordPage );
    }

    private doFacebookSignup(){
        console.log( "Go to login with Facebook" );
    }


}
