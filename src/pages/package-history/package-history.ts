import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'page-package-history',
  templateUrl: 'package-history.html',
})
export class PackageHistoryPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackageHistoryPage');
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
