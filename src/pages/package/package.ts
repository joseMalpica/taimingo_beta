import { Component } from '@angular/core';
import { ModalController, NavController, NavParams } from 'ionic-angular';
//pages
import { CollectionCodePage, SharePackagePage, ShowPucPage, SendOtherPucPage, HandDeliveryPage } from "../index.pages";

//providers
import { Identity } from "../../providers/identity";
import { FreedomApi } from "../../providers/freedom-api";

//plugins
import { Storage } from '@ionic/storage';
import {InfoModalPage} from "../info-modal/info-modal";

@Component({
    selector: 'page-package',
    templateUrl: 'package.html',
})
export class PackagePage {

    private package:any = null;
    private packId;any = null;
    private pos:any = null;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public storage: Storage,
                public modalCtrl: ModalController,
                private _fa: FreedomApi) {
        this.packId = this.navParams.get("packId");
        this.getPackage(  );
        this.pos = this.navParams.get("myPos");
    }

    getPackage(  ){
        this._fa.get('taimingo/package/get-package?id='+this.packId).subscribe((data)=>{
            this.package = data.data;
            //console.log( data.data );
        });
    }


    ionViewCanEnter(){
        this.storage.ready().then(() =>{
            this.storage.get('loginState').then((value)=>{
                if( value === true ){
                    return true;
                }else{
                    return false;
                }
            });
        });
    }


    ionViewDidLoad() {
        //console.log('ionViewDidLoad PackagePage');

    }

    //------Modales para mostrar informacion (ayuda de primerisos)
    handDeliveryInfo() {
        let title = "Entrega en mano";
        let modal = this.modalCtrl.create(InfoModalPage, {title: title});
        modal.present();
    }


    //-----Navigation

    public goSharePackage(){
        //this.navCtrl.push( SharePackagePage, { "puc": this.puc, "myPos": this.pos, "pack": this.package } );
    }

    public goCollectionCode(){
        this._fa.get('taimingo/pick-up-center/get-puc?id=' + this.package.puc_id).subscribe((data)=>{
            this.navCtrl.push( CollectionCodePage, { "puc": data.data.puc, "myPos": this.pos, "image": this.package.image, "packId": this.package.id });
        });
    }

    goHandDelivery(){
        //this.handDeliveryInfo();
        this._fa.get('taimingo/pick-up-center/get-puc?id=' + this.package.puc_id).subscribe((data)=>{
            this.navCtrl.push( HandDeliveryPage, { "myPos": this.pos, "puc": data.data.puc, "package": this.package } );
        });
    }
    public goPuc(){
        this.navCtrl.push( ShowPucPage, { "pucId": this.package.puc_id, "myPos": this.pos } )
    }

    public goSend(){
        this._fa.get('taimingo/pick-up-center/get-puc?id=' + this.package.puc_id).subscribe((data)=>{
            this.navCtrl.push( SendOtherPucPage, { "myPos": this.pos, "puc": data.data.puc, 'package': this.package } );
        });
    }

}
