import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
//pages
import { ProfilePage } from "../index.pages";

//------providers
import { Identity } from "../../providers/identity";

@Component({
  selector: 'page-profile-view',
  templateUrl: 'profile-view.html',
})
export class ProfileViewPage {

  private nickname: any;
  private name: any;
  private surnames: any;
  private phone: any;
  private email: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public app: App,
              private _id: Identity) {
              this.nickname = _id.nickname;
              this.name     = _id.name;
              this.surnames = _id.surnames;
              this.phone    = _id.phone;
              this.email    = _id.email;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfileViewPage');
  }

  goProfileEdit(){
    console.log( "Go out" );
    this.navCtrl.push(ProfilePage);
    //this.app.getRootNav().push( ProfilePage);
  }
}
