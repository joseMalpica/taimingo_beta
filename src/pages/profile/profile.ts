import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormControl } from '@angular/forms';
import { SettingsModel } from './settings.model';
import { SettingsService } from './settings.service';
//providers
import { Identity } from "../../providers/identity";
import { FreedomApi } from "../../providers/freedom-api";
//pages
import { AddCardPage } from "../index.pages";
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
  providers: [SettingsService]
})
export class ProfilePage {

  settingsForm: FormGroup;
  img = this.identity.photo_circle;
  newsletter: number = 0;
  loading:any;
  //for gender
  testRadioOpen:any;
  testRadioResult:any;
  listing:SettingsModel = new SettingsModel();

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingCtrl:LoadingController,
              private alertCtrl: AlertController,
              private api: FreedomApi,
              private settingsService: SettingsService,
              private identity: Identity) {
                this.loading = this.loadingCtrl.create();
                this.settingsForm = new FormGroup({
                      name: new FormControl(),
                      surnames: new FormControl(),
                      email: new FormControl(),
                      nickname: new FormControl(),
                      phone: new FormControl(),
                    });
  }

  ionViewDidLoad() {
    this.settingsForm.setValue({
          name:     this.identity.name,
          surnames: this.identity.surnames,
          email:    this.identity.email,
          nickname: this.identity.nickname,
          phone:    this.identity.phone,
    });
  }

  public saveData(){
    let prompt = this.alertCtrl.create({
          title: 'Contraseña',
          message: "Ingresa tu contraseña para confirmar tus cambios",
          inputs: [
            {
              name: 'password',
              placeholder: ' contraseña',
              type: 'password'
            },
          ],
          buttons: [
            {
              text: 'Cancelar',
              handler: data => {
                console.log( "Cancel clicked! " );
              }
            },
            {
              text: 'Guardar',
              handler: data => {
                this.delegateSentToApi( data );
              }
            }
          ]
        });
        prompt.present();
  }

  public delegateSentToApi( data: any ){
    //data.password (Guardar daros)
    this.loading.present();
    this.api.put('app/customer/save', { Customer: {
                name:          this.settingsForm.get('name').value,
                surnames:      this.settingsForm.get('surnames').value,
                email:         this.settingsForm.get('email').value,
                email_confirm: this.settingsForm.get('email').value,
                nickname:      this.settingsForm.get('nickname').value,
                password:      data.password,
                phone:         this.settingsForm.get('phone').value,
            }
        }
    ).subscribe((response)=>{
      console.log( response );
      if( response.ok == true ){
        this.identity.loadData(response.data);
      }else{
        this.showAlert( "Ajustes", "Eso no ha funcionado, por favor verifica tu contraseña" );
      }
    },(error)=>{
      this.showAlert( "Ajustes", "Error interno!" );
    });
    this.loading.dismiss();
    //this.settingsForm.status();
    //document.querySelector("#buttonSave")['style'].display = 'none';
    //(Guardar imagen)
  }

  logout(){
    this.identity.logout()
  }
  goAddCard(){
    this.navCtrl.push( AddCardPage );
  }

  private showAlert( title: string, msg: string ){
    let alert = this.alertCtrl.create({
          title: title,
          subTitle: msg,
          buttons: ['OK']
    });
    alert.present();
  }

}
