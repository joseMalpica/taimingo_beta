import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import {LoadingController} from "ionic-angular/index";
import { Storage } from '@ionic/storage';
//pages
import { HomePage } from "../index.pages";
//providers
import { FreedomApi } from "../../providers/freedom-api";
import { Identity } from "../../providers/identity";
@Component({
  selector: 'page-root',
  templateUrl: 'root.html',
})
export class RootPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public app:App,
              private storageSession:Storage,
              public loadingCtrl:LoadingController,
              private _fa: FreedomApi,
              private identity: Identity) {
  }

  ionViewDidLoad() {
    //console.log('ionViewDidLoad RootPage');
  }

}
