import { identifierModuleUrl } from '@angular/compiler/compiler';
import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActionSheetController, AlertController, App, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';

//-----plugins
import { Storage } from '@ionic/storage';

//------providers
import { FreedomApi } from "../../providers/freedom-api";

//------pages
import { ShowPucPage } from "../index.pages";

declare var google: any;

@Component({
    selector: 'page-send-other-puc',
    templateUrl: 'send-other-puc.html',
})
export class SendOtherPucPage {
    @ViewChild('map') mapElement: ElementRef;
    @ViewChild('searchbar', { read: ElementRef }) searchbar: ElementRef;
    addressElement: HTMLInputElement = null;
    listSearch: string = '';
    map: any;
    marker: any;
    loading: any;
    search: boolean = false;
    error: any;
    switch: string = "map";
    private pucs:any;
    private puc:any;
    private pos:any;
    private package:any;
    private infoWindow;

    constructor(
        private navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public app: App,
        public nav: NavController,
        public navParams: NavParams,
        public zone: NgZone,
        public platform: Platform,
        public alertCtrl: AlertController,
        public storage: Storage,
        public actionSheetCtrl: ActionSheetController,
        public geolocation: Geolocation,
        public _fa: FreedomApi
    ) {
        this.pos = this.navParams.get("myPos");
        this.puc = this.navParams.get("puc");
        this.package = this.navParams.get('package');
        this.platform.ready().then(() => {
            this.loadMaps();
        });

    }

    ionViewDidLoad(){
        document.getElementById("btnSendPack").style.display = "none";
        document.getElementById("btnAddPuc").style.display = "none";
    }

    public getPucs(){
        console.log(this.puc);
        this._fa.get( 'taimingo/pick-up-center' ).subscribe(( data )=>{
            //console.log( data.data.models );
            this.pucs = data.data.models;
            this.pucs.forEach( puc => {
                if(this.puc._id != puc._id){
                    this.addMarker(puc.position, puc.name, true, puc);
                }
            });
        });
    }

    loadMaps() {
        if (!!google) {
            this.getPucs();
            this.initializeMap();
            //this.getCurrentPosition();
            this.loading = this.loadingCtrl.create({
                content: 'Searching Location ...'
            });
            this.loading.present();
            setTimeout(() => {
                let myPos = new google.maps.LatLng(this.pos.lat, this.pos.lng);
                let options = {
                    center: myPos,
                    zoom: 12
                };
                this.map.setOptions(options);
                this.addMarker(myPos, "My posicion");
                this.loading.dismiss();
            }, 2000);
            this.initAutocomplete();
        } else {
            this.errorAlert('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
        }
    }

    errorAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        this.loadMaps();
                    }
                }
            ]
        });
        alert.present();
    }

    initAutocomplete(): void {
        // reference : https://github.com/driftyco/ionic/issues/7223
        this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
        this.createAutocomplete(this.addressElement).subscribe((location) => {
            //console.log('Searchdata', location);
            let options = {
                center: location,
                zoom: 10
            };
            this.map.setOptions(options);
            this.addMarker(location, "Mi busqueda");
        });
    }

    createAutocomplete(addressEl: HTMLInputElement): Observable<any> {
        const autocomplete = new google.maps.places.Autocomplete(addressEl);
        autocomplete.bindTo('bounds', this.map);
        return new Observable((sub: any) => {
            google.maps.event.addListener(autocomplete, 'place_changed', () => {
                const place = autocomplete.getPlace();
                if (!place.geometry) {
                    sub.error({
                        message: 'Autocomplete returned place with no geometry'
                    });
                } else {
                    //console.log('Search Lat', place.geometry.location.lat());
                    //console.log('Search Lng', place.geometry.location.lng());
                    sub.next(place.geometry.location);
                    //sub.complete();
                }
            });
        });
    }

    initializeMap() {
        var myPosition: any;
        this.zone.run(() => {
            var mapEle = this.mapElement.nativeElement;
            this.geolocation.getCurrentPosition()
                .then(position => {
                    myPosition = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    }
                })
                .catch(error=>{
                    console.log(error);
                });
            this.map = new google.maps.Map(mapEle, {
                zoom: 12,
                center: myPosition,
            });

            let markers = [];
            google.maps.event.addListenerOnce(this.map, 'idle', () => {
                google.maps.event.trigger(this.map, 'resize');
                mapEle.classList.add('show-map');
                this.bounceMap(markers);
                this.getCurrentPositionfromStorage(markers)
            });
            google.maps.event.addListener(this.map, 'bounds_changed', () => {
                this.zone.run(() => {
                    this.resizeMap();
                });
            });
        });
    }

    //http://stackoverflow.com/questions/19304574/center-set-zoom-of-map-to-cover-all-visible-markers
    bounceMap(markers) {
        let bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        this.map.fitBounds(bounds);
    }
    resizeMap() {
        setTimeout(() => {
            google.maps.event.trigger(this.map, 'resize');
        }, 200);
    }

    getCurrentPositionfromStorage(markers) {
        this.storage.get('lastLocation').then((result) => {
            if (result) {
                let myPos = new google.maps.LatLng(result.lat, result.long);
                this.map.setOptions({
                    center: myPos,
                    zoom: 14
                });
                let marker = this.addMarker(myPos, "My last saved Location: " + result.location);
                markers.push(marker);
                //this.bounceMap(markers);
                this.resizeMap();
            }
        });
    }

    showToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }

    // go show currrent location
    getCurrentPosition() {
        this.loading = this.loadingCtrl.create({
            content: 'Searching Location ...'
        });
        this.loading.present();
        let locationOptions = { timeout: 10000, enableHighAccuracy: true };
        this.geolocation.getCurrentPosition(locationOptions)
            .then(position => {
                    this.loading.dismiss().then(() => {
                        //console.log(position.coords.latitude, position.coords.longitude);
                        let myPos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                        let options = {
                            center: myPos,
                            zoom: 12
                        };
                        this.map.setOptions(options);
                        this.addMarker(myPos, "My posicion");
                    });
                },
                (error) => {
                    this.loading.dismiss().then(() => {
                        this.showToast('Location not found. Please enable your GPS!');
                        console.log(error);
                    });
                }
            )
    }

    addMarker(position, content, isPuc?, puc?) {
        var pinColor; //verde
        (isPuc == true ?  pinColor = "0DB605"  :  pinColor = "FE7569");
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: position,
            icon: pinImage,
        });
        if( isPuc ){
            this.addInfoWindowPuc(marker, content, puc);
        } else{
            this.addInfoWindow(marker, content);
        }
        return marker;
    }

    addInfoWindow(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: "<strong> "+ content + " </strong>"
        });
        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });
    }

    public  hola( action ){
        //action
        //1->addPuc
        //2->sendPack
        var newPuc:any;
        //get pucId press
        var pucId:any;

        if(action == 1){
            pucId = document.getElementById('btnAddPuc').getAttribute("value");
            //get puc
            this.pucs.forEach((puc)=>{
                if(puc._id == pucId){
                    newPuc = puc;
                }
            });
            //go addPuc
        }else{
            pucId = document.getElementById('btnSendPack').getAttribute("value");
            //get puc
            this.pucs.forEach((puc)=>{
                if(puc._id == pucId){
                    newPuc = puc;
                }
            });
            this.movePack( newPuc );
        }
    }

    addInfoWindowPuc(marker, content, puc?) {
        var infoWindow = null;
        if(puc.aliasPuc == null){
            infoWindow = new google.maps.InfoWindow({
                content: "<strong> "+ content + " </strong> <br>" +
                "<span id='addPuc' style='color: #0DB605;float: left;font-size: 14px;font-weight: bold;'> Agregar PUC </span>"+
                "<span id='sendPack' style='color: #FF7043;float: right;font-size: 14px;font-weight: bold;' > Enviar! </span>"
            });
        }else{
            infoWindow = new google.maps.InfoWindow({
                content: "<strong> "+ content + " </strong> <br>" +
                "<span id='addPuc' style='color: #0DB605;float: left;font-size: 14px;font-weight: bold;'> Ir al PUC </span>"+
                "<span id='sendPack' style='color: #FF7043;float: right;font-size: 14px;font-weight: bold;' > Enviar! </span>"
            });
        }

        google.maps.event.addListener(marker, 'click', () => {
            //si hay ventana
            if(this.infoWindow != null){
                this.infoWindow.close();
            }
            this.infoWindow = infoWindow;
            this.infoWindow.open(this.map, marker);
            var btnAddPuc = document.getElementById('addPuc');
            btnAddPuc.onclick = function(){
                document.getElementById('btnAddPuc').setAttribute("value", puc._id);
                document.getElementById('btnAddPuc').click();

            };
            var btnSendPack = document.getElementById('sendPack');
            btnSendPack.onclick = function(){
                document.getElementById('btnSendPack').setAttribute("value", puc._id);
                document.getElementById('btnSendPack').click();

            };
            //infoWindow.close();
        });

    }

    public movePack( newPuc ){
        let confirm = this.alertCtrl.create({
            title: 'Mover paquete',
            message: 'Tu paquete sera enviado del PUC: <strong>'+this.puc.name+'</strong> al PUC: <strong>'+ newPuc.name
            +'</strong><br> Costo: $15.00 MX',
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        console.log('Disagree clicked');
                    }
                },
                {
                    text: 'Ok',
                    handler: () => {
                        let origin_puc = this.puc._id;
                        let destination_puc = newPuc._id;
                        let packageId = this.package.id;
                        console.log('origin_puc: '+origin_puc );
                        console.log('destination_puc: '+destination_puc );
                        console.log('packageId: '+packageId );
                        this._fa.post('taimingo/package-transfer',{'PackageTransfer':{
                                        'origin_pickup_center_id': origin_puc,
                                        'destination_pickup_center_id': destination_puc,
                                        'package_id': packageId,
                                        'note': ''
                                        }
                                    }
                        ).subscribe((data)=>{
                            console.log(data.data);
                            this.navCtrl.pop();
                        });

                    }
                }
            ]
        });
        confirm.present();
    }



    //---Navigation
    public goShowPucPage( puc ){
        //this.app.getRootNav().push( ShowPucPage, { puc } );
        this.navCtrl.push( ShowPucPage, { puc , "myPos": this.pos } );
    }

}
