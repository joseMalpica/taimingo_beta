import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';

//plugins
import { Storage } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing';

//------pages
import { ShowPucPage } from "../index.pages";

//-----Providers
import {FreedomApi} from "../../providers/freedom-api";

declare let cordova: any;

@Component({
    selector: 'page-share-package',
    templateUrl: 'share-package.html',
})
export class SharePackagePage {

    private packId:any;
    private pos:any;
    private puc:any;
    private name:any;
    private surnames:any;
    private phone:any;
    private email:any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private socialSharing: SocialSharing,
                public toastCtrl: ToastController,
                private _fa: FreedomApi,
                public storage: Storage) {
        this.pos = this.navParams.get("myPos");
        this.puc = this.navParams.get("puc");
        this.packId = navParams.get( "packId" );


        console.log(this.packId);
        //console.log( this.package );
    }

    ionViewDidLoad() {

    }

    ionViewCanEnter(){
        this.storage.ready().then(() =>{
            this.storage.get('loginState').then((value)=>{
                if( value === true ){
                    return true;
                }else{
                    return false;
                }
            });
        });
    }

    public share(){

        let msg = this.name + " te comparto mi código Taimingo, recoge mi paquete en: " + this.puc.address;
        let subject = "Compartir código";
        const file = cordova.file.dataDirectory + 'imagen.jpg';
        //this.presentToast(file);
        this.socialSharing.share(msg,subject,file, null)
            .then(() => {
                //this.presentToast( "ok!" );
                this.sendLog();
                this.navCtrl.pop();
            })
            .catch((err) => {
                this.presentToast( err );
            });
    }

    presentToast( msg ) {
        let toast = this.toastCtrl.create({
            message: msg,
            //duration: 3000,
            showCloseButton: true
        });
        toast.present();
    }

    sendLog(){
        this._fa.post('taimingo/package-shared',
            {'PackageShared':{
                    'package_id': this.packId,
                    'name': this.name,
                    'surnames': this.surnames,
                    'cellphone': this.phone,
                    'email': this.email,
                }
            }).subscribe((data)=>{
            //this.presentToast(data);

        });
    }


    //--------Navigation
    public goPuc(){
        console.log(this.puc);
        this.navCtrl.push( ShowPucPage, { "pucId": this.puc._id, "myPos": this.pos } );
    }
}
