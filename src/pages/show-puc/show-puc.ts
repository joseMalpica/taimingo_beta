
import { identifierModuleUrl } from '@angular/compiler/compiler';
import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { ActionSheetController, AlertController, App, LoadingController, NavController, NavParams, Platform, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Observable } from 'rxjs/Observable';

//-----plugins
import { Storage } from '@ionic/storage';

//------providers
import { FreedomApi } from "../../providers/freedom-api";

//-----pages
import { PackagePage } from "../index.pages";

declare var google: any;

@Component({
    selector: 'page-show-puc',
    templateUrl: 'show-puc.html',
})
export class ShowPucPage {
    @ViewChild('map') mapElement: ElementRef;

    map: any;
    marker: any;
    loading: any;
    search: boolean = false;
    error: any;
    switch: string = "map";
    private pucs:any;
    packages: any;
    private puc: any;
    private pos;
    private newAlias:any;
    private disableBtn: boolean = true;

    constructor(
        private navCtrl: NavController,
        private navParams: NavParams,
        public loadingCtrl: LoadingController,
        public toastCtrl: ToastController,
        public app: App,
        public nav: NavController,
        public zone: NgZone,
        public platform: Platform,
        public alertCtrl: AlertController,
        public storage: Storage,
        public actionSheetCtrl: ActionSheetController,
        public geolocation: Geolocation,
        public _fa: FreedomApi) {


    }

    ionViewDidLoad(){
        this.platform.ready().then(() => {
            this.pos = this.navParams.get("myPos");
            let pucId = this.navParams.get("pucId");
            this._fa.get( 'taimingo/pick-up-center/get-puc?id=' + pucId ).subscribe(( data )=>{
                this.puc = data.data.puc;
                this.packages = data.data.packages;
                console.log(this.puc);
                console.log(this.packages);
                this.newAlias =  this.puc.aliasPuc;
                this.loadMaps();
            });

        });
    }

    loadMaps() {
        if (!!google) {
            this.initializeMap();
            this.getCurrentPosition();
            this.addMarker(this.puc.position, this.puc.name, true);
            //this.initAutocomplete();
        } else {
            this.errorAlert('Error', 'Something went wrong with the Internet Connection. Please check your Internet.')
        }
    }

    errorAlert(title, message) {
        let alert = this.alertCtrl.create({
            title: title,
            message: message,
            buttons: [
                {
                    text: 'OK',
                    handler: data => {
                        this.loadMaps();
                    }
                }
            ]
        });
        alert.present();
    }

    initializeMap() {
        this.zone.run(() => {
            var mapEle = this.mapElement.nativeElement;
            this.map = new google.maps.Map(mapEle, {
                zoom: 12,
                center: new google.maps.LatLng(this.pos.lat, this.pos.lng),
            });
            let markers = [];
            google.maps.event.addListenerOnce(this.map, 'idle', () => {
                google.maps.event.trigger(this.map, 'resize');
                mapEle.classList.add('show-map');
                this.bounceMap(markers);
            });

            google.maps.event.addListener(this.map, 'bounds_changed', () => {
                this.zone.run(() => {
                    this.resizeMap();
                });
            });
        });
    }

    //http://stackoverflow.com/questions/19304574/center-set-zoom-of-map-to-cover-all-visible-markers
    bounceMap(markers) {
        let bounds = new google.maps.LatLngBounds();
        for (var i = 0; i < markers.length; i++) {
            bounds.extend(markers[i].getPosition());
        }
        this.map.fitBounds(bounds);
    }
    resizeMap() {
        setTimeout(() => {
            google.maps.event.trigger(this.map, 'resize');
        }, 200);
    }

    showToast(message) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000
        });
        toast.present();
    }

    // go show currrent location
    getCurrentPosition() {
        this.loading = this.loadingCtrl.create({
            content: 'Searching Location ...'
        });
        this.loading.present();
        setTimeout(() => {
            let myPos = new google.maps.LatLng(this.pos.lat, this.pos.lng);
            let options = {
                center: this.puc.position,
                zoom: 12
            };
            this.map.setOptions(options);
            this.addMarker(myPos, "Mi posición");
            this.loading.dismiss();
        }, 2000);
    }

    addMarker(position, content, isPuc?) {
        var pinColor; //verde
        (isPuc == true ?  pinColor = "0DB605"  :  pinColor = "FE7569");
        var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
            new google.maps.Size(21, 34),
            new google.maps.Point(0,0),
            new google.maps.Point(10, 34));
        let marker = new google.maps.Marker({
            map: this.map,
            animation: google.maps.Animation.DROP,
            position: position,
            icon: pinImage
        });
        if( isPuc ){
            this.addInfoWindowPuc(marker, content);
        } else{
            this.addInfoWindow(marker, content);
        }
        return marker;
    }

    addInfoWindow(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: "<strong> "+ content + " </strong>"
        });
        google.maps.event.addListener(marker, 'click', () => {
            infoWindow.open(this.map, marker);
        });
    }

    public  hola(){
        console.log("Hola pepe!");
    }

    addInfoWindowPuc(marker, content) {
        let infoWindow = new google.maps.InfoWindow({
            content: "<strong> "+ content + " </strong> <br>"
        });
        google.maps.event.addListener(marker, 'click', () => {

            infoWindow.open(this.map, marker);
            //infoWindow.close();
        });
    }

    onChangeTime(data) : void {
        this.disableBtn = null;
    }

    public addPuc(){
        console.log("Agregando PUC");
        console.log("PUC id: " + this.puc._id);
        console.log("Alias: " + this.newAlias);
        this._fa.post('taimingo/pick-up-center-customer',{PickUpCenterCustomer:{
            pick_up_center_id:this.puc._id,
            alias: this.newAlias
        }}).subscribe((res)=>{
            console.log( res );
        },err=>{
            console.log( err );
        });
    }

    public updatePuc(){
        console.log("Editar PUC");
        console.log("Link: " + 'taimingo/pick-up-center-customer/update?id='+this.puc.aliasPuc.alias_id);
        console.log("PUC id: " + this.puc.aliasPuc.alias_id);
        console.log("Alias: " + this.newAlias);
        this._fa.put('taimingo/pick-up-center-customer/update?id='+this.puc.aliasPuc.alias_id,{PickUpCenterCustomer:{
            alias: this.newAlias
        }}).subscribe((res)=>{
            console.log( res );
        },err=>{
            console.log( err );
        });
    }

    //---Navigation
    public goShowPucPage( puc ){
        //this.app.getRootNav().push( ShowPucPage, { puc } );
        this.navCtrl.push( ShowPucPage, { puc} );
    }

    public gotPackage( puc, pack ){
        //this.navCtrl.push( PackagePage, {pack} );
        //this.app.getActiveNav().parent.parent.setRoot( PackagePage );
        this.navCtrl.push( PackagePage, { puc , pack , "myPos": this.pos } );
    }

}
