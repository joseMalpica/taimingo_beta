import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Validators, FormGroup, FormControl,  FormBuilder } from '@angular/forms';

//-----pages
import { ValidateSmsPage } from "../index.pages";

//providers
import { FreedomApi } from "../../providers/freedom-api";

@Component({
    selector: 'page-signup',
    templateUrl: 'signup.html',
})
export class SignupPage {

    signupForm: FormGroup;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public formBuilder: FormBuilder,
                private api: FreedomApi) {
        this.signupForm = formBuilder.group({
            name: ['', Validators.compose([Validators.required])],
            lastnames: ['', Validators.compose([Validators.required])],
            phone: ['', Validators.compose([Validators.required])],
            email: ['', Validators.compose([Validators.required])],
            password: ['', Validators.compose([Validators.required])],
            birthday: ['', Validators.compose([Validators.required])],
            gender: ['', Validators.compose([Validators.required])]
        });
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SingupPage');
    }

    doSignup(){
        let data: any = " { \"Customer\": {"+
            "\"name\": \""+ this.signupForm.get('name').value+ "\""+
            ", \"surnames\": \""+ this.signupForm.get('lastnames').value+ "\""+
            ", \"email\": \""+ this.signupForm.get('email').value+ "\""+
            ", \"email_confirm\": \""+ this.signupForm.get('email').value+ "\""+
            ", \"password\": \""+ this.signupForm.get('password').value+ "\""+
            ", \"phone\": \""+ this.signupForm.get('phone').value+ "\""+
            ", \"birthday\": \""+ this.signupForm.get('birthday').value+ "\""+
            ", \"gender\": \""+ this.signupForm.get('gender').value+ "\""+
            "} }";

        console.log( data );
        //this.navCtrl.push( ValidateSmsPage, {"phone": this.signupForm.get('phone').value} );


        this.api.post('app/guest/register', data)
                  .subscribe((response)=>{
                      console.log( response );
                      this.navCtrl.push( ValidateSmsPage );
                  },(err)=>{
                    console.log( err );
        });

        //this.navCtrl.push( AddCardPage );
    }

}
