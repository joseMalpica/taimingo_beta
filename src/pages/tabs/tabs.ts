import { Component } from '@angular/core';
//pages
import { HomePage, ProfileViewPage } from "../index.pages";
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {

  tab1: any;
  tab4: any;

  constructor() {
    this.tab1 = HomePage;
    this.tab4 = ProfileViewPage;
  }

}
