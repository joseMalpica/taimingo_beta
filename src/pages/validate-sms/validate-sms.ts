import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-validate-sms',
  templateUrl: 'validate-sms.html',
})
export class ValidateSmsPage {

  private phone;
  private view = 1;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.phone = this.navParams.get("phone");
  }

  ionViewDidLoad() {

  }

  updatePhone(){
    this.view = 2;
  }
  cancelUpdatePhone(){
    this.view = 1;
  }

}
