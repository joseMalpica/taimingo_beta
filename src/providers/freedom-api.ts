import { Injectable } from '@angular/core';
import { Http,Response,URLSearchParams  } from '@angular/http';
import { Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {PartialObserver} from "rxjs/Observer";
import {Identity} from "./identity";
import {Observable} from "rxjs/Observable";
import {isObject} from "ionic-angular/es2015/util/util";
import {AlertController} from "ionic-angular/index";
import {Events} from "ionic-angular/index";

/*
 Generated class for the FreedomApi provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class FreedomApi {

    public baseUrl:string = 'http://52.15.95.36/taimingo/public/api/';

    constructor(public http:Http,
                public identity:Identity,
                private alertCtrl:AlertController,
                public events:Events) {
        this.identity._fa = this;
    }

    post(path, data) {

        let headers = this.prepareHeaders();

        let url = this.baseUrl + path;

        //noinspection TypeScriptUnresolvedFunction
        let promise = this.http.post(
            url,
            data,
            {
                headers: headers
            }
        ).map(res => {
                return this.processResponse(res)
            });

        return promise;
    }

    get(path, data?:any) {

        let headers = this.prepareHeaders();

        //console.log(headers);

        let url = this.baseUrl + path;
        //noinspection TypeScriptUnresolvedFunction
        let promise = this.http.get(
            url,
            {
                headers: headers,
                search: this.queryBuild(data)
            }
        ).map(res => {
                return this.processResponse(res)
            });

        return promise;
    }

    put(path, data) {

        let headers = this.prepareHeaders();

        let url = this.baseUrl + path;

        //noinspection TypeScriptUnresolvedFunction
        let promise = this.http.put(
            url,
            data,
            {
                headers: headers,
            }
        ).map(res => {
                return this.processResponse(res)
            });

        return promise;
    }

    'delete'(path, data) {

        let headers = this.prepareHeaders();

        let url = this.baseUrl + path;

        //noinspection TypeScriptUnresolvedFunction
        let promise = this.http.delete(
            url,
            {
                headers: headers,
                search: this.queryBuild(data)
            }
        ).map(res => {
                return this.processResponse(res)
            });

        return promise;
    }

    public processResponse(res) {
        try {
            let response = res.json();
            if ("ok" in response) {

                if ('terminate' in response) {
                    if (response.terminate) {
                        this.identity.seppoku();
                    }
                }

                for (var type in response.flashes) {
                    let alert = this.alertCtrl.create({
                        cssClass: 'alert-' + type,
                        message: response.flashes[type].join("\n"),
                        buttons: ['Entendido']
                    });
                    alert.present().catch(() => {
                    });
                }

                return this.eventTrigger(
                    response,
                    {
                        ok: response.ok,
                        data: 'data' in response ? response.data : {},
                    }
                );

            } else {

                return this.eventTrigger(
                    response,
                    {
                        ok: false,
                        data: {},
                    }
                );

            }
        } catch (exception) {
            return {
                ok: false,
                data: {}
            }
        }
    }

    queryBuild(object:any, params?:URLSearchParams, parentName?:any) {
        if (!params) {
            params = new URLSearchParams();
        }

        for (var property in object) {
            let value = object[property];
            if (typeof value == "object") {
                if (!parentName) {
                    this.queryBuild1(value, params, property);
                } else {
                    this.queryBuild1(value, params, parentName + '[' + property + ']');
                }
            } else {
                if (!parentName) {
                    params.set(property, value);
                } else {
                    params.set(parentName + '[' + property + ']', value);
                }
            }
        }

        return params;
    }

    queryBuild1(object:any, params:URLSearchParams, parentName:string) {
        for (var property in object) {
            let value = object[property];
            if (typeof value == "object") {
                if (!parentName) {
                    this.queryBuild2(value, params, property);
                } else {
                    this.queryBuild2(value, params, parentName + '[' + property + ']');
                }
            } else {
                if (!parentName) {
                    params.set(property, value);
                } else {
                    params.set(parentName + '[' + property + ']', value);
                }
            }
        }

        return params;
    }

    queryBuild2(object:any, params:URLSearchParams, parentName:string) {
        for (var property in object) {
            let value = object[property];
            if (typeof value == "object") {
                if (!parentName) {
                    this.queryBuild3(value, params, property);
                } else {
                    this.queryBuild3(value, params, parentName + '[' + property + ']');
                }
            } else {
                if (!parentName) {
                    params.set(property, value);
                } else {
                    params.set(parentName + '[' + property + ']', value);
                }
            }
        }

        return params;
    }

    queryBuild3(object:any, params:URLSearchParams, parentName:string) {
        for (var property in object) {
            let value = object[property];
            if (typeof value == "object") {
                if (!parentName) {
                    this.queryBuild4(value, params, property);
                } else {
                    this.queryBuild4(value, params, parentName + '[' + property + ']');
                }
            } else {
                if (!parentName) {
                    params.set(property, value);
                } else {
                    params.set(parentName + '[' + property + ']', value);
                }
            }
        }

        return params;
    }

    queryBuild4(object:any, params:URLSearchParams, parentName:string) {
        for (var property in object) {
            let value = object[property];
            if (!parentName) {
                params.set(property, value);
            } else {
                params.set(parentName + '[' + property + ']', value);
            }
        }

        return params;
    }

    /**
     *
     * @param response
     * @param data
     * @returns {any}
     */
    eventTrigger(response:any, data:any) {
        if ("action" in response) {
            this.events.publish(response.action, response.action, data);
        }

        return data;
    }

    prepareHeaders() {
        let headers = new Headers();

        if (this.identity.isLogged()) {
            headers.append(this.identity.jwt.name, this.identity.jwt.token);
        } else {
            headers.append('x-guest-jwt', 'guest');
        }

        headers.append('Content-Type', 'application/json; charset=UTF-8');

        return headers;
    }
}
