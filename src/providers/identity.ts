import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { LoginPage } from '../pages/login/login';
import {App} from "ionic-angular/index";
import {FreedomApi} from "./freedom-api";
import { Events } from 'ionic-angular';

/*
 Generated class for the Identity provider.

 See https://angular.io/docs/ts/latest/guide/dependency-injection.html
 for more info on providers and Angular 2 DI.
 */
@Injectable()
export class Identity {

    public loginState:boolean = false;
    public data:any = {};

    public id:string = '';
    public nickname:string = '';
    public email:string = '';
    public phone:string = '';
    public name:string = '';
    public surnames:string = '';
    public birthday:string = '';
    public gender:number = 0;
    public newsletter:boolean = false;
    public photo_circle:string = '';
    public photo_icon:string = '';
    public photo_thumbnail:string = '';
    public utc_offset:number = 0;
    public timezone:number =0;
    public jwt:{name:string,token:string} = {name: '', token: ''};
    /**
     * FreedomApi se auto asigna en el constructor de FreedomApi
     */
    public _fa:FreedomApi;


    public main_page:{ component: any };
    public login_page:{ component: any };

    constructor(public app:App,
                public storage:Storage,

                public events:Events) {

        this.storage.get('identity').then(
            data => {
                this.loadData(data, false);
                if (this.isLogged()) {

                    this._fa.get('app/guest/valid');

                    /*
                    this._fa.get('app/guest/valid').subscribe(
                        (response) => {
                            console.log(response);
                            if (response.data != true) {
                                this.seppoku();
                            }
                            this.storage.set('loginState',true);
                            events.publish('identity:after_validate_identity', response.data);
                        },
                        error => {
                          this.storage.remove('identity').then(value => {
                            this.seppoku();
                            this.storage.set('loginState',false)

                          });
                        });
                       */
                }

            }
        );

    }

    loadData(data?:any, saveData:boolean = true) {

        if (!data || !('Customer' in data) || !('JWT' in data)) {
            return;
        }

        if (saveData) {
            this.storage.set('identity', data);
        }

        this.id = '_id' in data.Customer ? data.Customer._id : null;
        this.nickname = 'nickname' in data.Customer ? data.Customer.nickname : null;
        this.email = 'email' in data.Customer ? data.Customer.email : null;
        this.phone = 'phone' in data.Customer ? data.Customer.phone : null;
        this.name = 'name' in data.Customer ? data.Customer.name : null;
        this.surnames = 'surnames' in data.Customer ? data.Customer.surnames : null;
        this.birthday = 'birthday' in data.Customer ? data.Customer.birthday : null;
        this.gender = 'gender' in data.Customer ? data.Customer.gender : null;
        this.newsletter = 'newsletter' in data.Customer ? data.Customer.newsletter : null;
        this.photo_circle = 'photo_circle' in data.Customer ? data.Customer.photo_circle : null;
        this.photo_thumbnail = 'photo_thumbnail' in data.Customer ? data.Customer.photo_thumbnail : null;
        this.photo_icon = 'photo_icon' in data.Customer ? data.Customer.photo_icon : null;
        this.utc_offset = 'utc_offset' in data.Customer ? data.Customer.utc_offset : null;
        this.timezone = 'timezone' in data.Customer ? data.Customer.timezone : null;
        this.jwt.name = data.JWT.name;
        this.jwt.token = data.JWT.token;

        this.loginState = true;

    }

    logout() {

        this._fa.get('app/customer/logout', {}).subscribe(
            response => {
                this.storage.remove('identity').then(value => {
                    this.seppoku();
                    this.events.publish('identity:after_logout');
                });
            },
            error => {
                console.log(error);
                this.storage.remove('identity').then(value => {
                  this.seppoku();
                  this.events.publish('identity:after_logout');
                });
            }
        );

    }

    isLogged() {
        return this.loginState;
    }

    check() {
        return this._fa.get('app/guest/valid');
    }

    seppoku() {

        this.events.publish('identity:before_seppoku');
        this.app.getActiveNav().parent.parent.setRoot( LoginPage );
        /*this.storage.clear().then(()=> {
            console.log("byebye")
          }
        );*/
        this.storage.remove('loginState').then(value => {
            this.events.publish('identity:after_logout');
        });

        this.loginState = false;
        this.data = {};
        this.id = '';
        this.nickname = '';
        this.email = '';
        this.phone = '';
        this.name = '';
        this.surnames = '';
        this.birthday = '';
        this.gender = 0;
        this.newsletter = false;
      //  this.photo_circle = '';
      //  this.photo_thumbnail = '';*/
        this.jwt = {name: '', token: ''};

        document.cookie.split(";").forEach(function(c) { document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/"); });

        this.loginState = false;

        this.events.publish('identity:after_seppoku');
    }
}
