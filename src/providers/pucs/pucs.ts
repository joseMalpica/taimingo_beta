import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class PucsProvider {


  constructor(public http: Http) {

  }

  getPucs(){
    let promesa = new Promise( (resolve, reject) =>{
      let url='./assets/data/pucs.json';
      this.http.get( url )
                .map( resp => resp.json() )
                .subscribe( (data) => {
                  resolve( data.pucs);
                } )
    } );
    return promesa;
  }

}
